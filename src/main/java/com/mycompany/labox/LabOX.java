/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.labox;

import java.util.Scanner;

/**
 *
 * @author sirikon
 */
public class LabOX {

    static char[][] table = {{'_', '_', '_'},
    {'_', '_', '_'},
    {'_', '_', '_'}};
    static char currentPlayer = 'X';
    static int row, col;

    public static void main(String[] args) {
        printWelcome();
        while (true) {
            printTable();
            printTurn();
            inputRowCol();

            if (isWin()) {
                printTable();
                printWin();
                break;
            }
            switchPlayer();
        }
    }

    private static void printWelcome() {
        System.out.println("Welcome to XOXO Game!");
    }

    private static void printTable() {
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[i].length; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }

    private static void printTurn() {
        System.out.println("Turn " + currentPlayer);
    }

    private static void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        while (true) {
            System.out.print("Please enter row and col: ");
            row = kb.nextInt();
            col = kb.nextInt();
            if (table[row - 1][col - 1] == '_') {
                table[row - 1][col - 1] = currentPlayer;
                break;
            }
            System.out.println("It's full already! You can't continue.");
        }
    }

    private static void switchPlayer() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }

    private static boolean isWin() {
        if (checkRow()) {
            return true;
        }
        if (checkCol()) {
            return true;
        }
        if (checkDiagonal()) {
            return true;
        }
        return false;
    }

    private static void printWin() {
        System.out.println("You win!!!");
    }

    private static boolean checkRow() {
        for (int i = 0; i < table.length; i++) {
            if (table[row - 1][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkDiagonal() {
        if (table[1-1][1-1] == currentPlayer && table[2-1][2-1] == currentPlayer && table[3-1][3-1] == currentPlayer 
            || table[1-1][3-1] == currentPlayer && table[2-1][2-1] == currentPlayer && table[3-1][1-1] == currentPlayer)
        {
            return true;
        }
        return false;      
    }
}
